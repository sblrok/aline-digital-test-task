const chai = require('chai');
const chaiHttp = require('chai-http');

chai.should();
chai.use(chaiHttp);

let server;
describe('Books controller tests', function() {
    this.timeout(15000);
    let countBooks = 0;

    before(async () => {
        server = require('../dist');
    });

     it('should get books COUNT on /books/count GET', function (done) {
         chai.request(server)
             .get('/books/info/count')
             .end((err, res) => {
                 res.should.have.status(200);
                 countBooks = res.body;
                 console.log(countBooks);
                 done();
             });
    });

    it('should list ALL books on /books GET', function (done) {
        chai.request(server)
            .get('/books')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.an('array');
                res.body.length.should.equal(countBooks);
                done();
            });
    });
    //
    it('should create one book on /books POST', function (done) {
        chai.request(server)
            .post('/books')
            .send({
                "title": "books_test_title" + (countBooks + 1),
                "date": "1990-08-31",
                "author": "books_test_author",
                "description": "books_test_description",
                "image": "books_test_image_url"
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.id.should.equal(countBooks + 1);
                res.body.title.should.equal('books_test_title' + (countBooks + 1));
                res.body.author.should.equal('books_test_author');
                res.body.description.should.equal('books_test_description');
                res.body.image.should.equal('books_test_image_url');
                done();
            });
    });

    it(`should get one book on /books/${countBooks + 1} GET`, function (done) {
        chai.request(server)
            .get('/books/' + (countBooks + 1))
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.id.should.equal(countBooks + 1);
                res.body.title.should.equal('books_test_title' + (countBooks + 1));
                res.body.author.should.equal('books_test_author');
                res.body.description.should.equal('books_test_description');
                res.body.image.should.equal('books_test_image_url');
                done();
            });
    });

    it(`should update one book on /books/${countBooks + 1} PUT`, function (done) {
        chai.request(server)
            .put('/books/' + (countBooks + 1))
            .send({
                "title": "new_books_test_title" + (countBooks + 1),
                "date": "1990-09-30",
                "author": "new_books_test_author",
                "description": "new_books_test_description",
                "image": "new_books_test_image_url"
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.id.should.equal((countBooks + 1).toString());
                res.body.title.should.equal('new_books_test_title' + (countBooks + 1));
                res.body.author.should.equal('new_books_test_author');
                res.body.description.should.equal('new_books_test_description');
                res.body.image.should.equal('new_books_test_image_url');
                done();
            });
    });

    it(`should find one book on /books/find POST`, function (done) {
        chai.request(server)
            .post('/books/find')
            .send({
                "offset": 0,
                "limit": 1,
                "findKey": "author",
                "findKey": "new_books_test_"
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.an('array');
                res.body.length.should.equal(1);
                // res.body[0].title.should.equal('new_books_test_title' + (countBooks + 1));
                res.body[0].author.should.equal('new_books_test_author');
                res.body[0].description.should.equal('new_books_test_description');
                res.body[0].image.should.equal('new_books_test_image_url');
                done();
            });
    });

    after(async () => {
        server.close();
    });
});
