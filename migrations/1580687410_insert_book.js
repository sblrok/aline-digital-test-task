module.exports = {
    up: 'insert into books ( title, date, author, description, image )\n' +
        'values ( \'test_title\', \'1990-08-31\', \'test_author\', \'test_description\', \'test_image_url\' );',
    down: 'truncate table books;'
};
