module.exports = {
    up: 'create table books\n' +
        '(\n' +
        'id int auto_increment,\n' +
        'title varchar(100) not null,\n' +
        'date date not null,\n' +
        'author text not null,\n' +
        'description text null,\n' +
        'image text null,\n' +
        'constraint books_pk primary key (id)\n' +
        ');\n' +
        '\n' +
        'create unique index books_id_uindex on books(id);\n' +
        '\n' +
        'create unique index books_title_uindex on books(title);\n',
    down: 'drop table books;'
};
