import HttpError from "../models/httpError.model";
import {logger} from '../logger';

export default async function errorHandlerMiddleware(ctx, next) {
    try {
        await next();
    } catch(err) {
        logger.error(err.message);
        logger.debug(err.stack);
        if (err instanceof HttpError) {
            ctx.throw(err.code, err.message);
        } else {
            ctx.throw(500, 'Internal Server Error');
        }
    }
}
