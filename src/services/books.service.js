import {container} from "../common/container";
import BooksRepository from "../repositories/books.repository";

export default class BooksService {
    constructor() {
        this.booksRepository = container.get(BooksRepository);
    }

    async find(options) {
        return this.booksRepository.find(options)
    }

    async findOne(options) {
        return this.booksRepository.findOne(options);
    }

    async create(data) {
        return this.booksRepository.create(data);
    }

    async update(id, data) {
        return this.booksRepository.update(id, data)
    }

    async delete(id) {
        return this.booksRepository.delete(id);
    }

    async getCount() {
        return this.booksRepository.getCount();
    }
}
