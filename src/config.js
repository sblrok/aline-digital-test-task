import convict from 'convict';
import * as dotenv from 'dotenv';

dotenv.config({path: '.env' + (process.env.NODE_ENV ? '.' + process.env.NODE_ENV : '')});

const values = convict({
    env: {
        doc: 'The application environment.',
        format: ['production', 'development', 'testing'],
        default: 'development',
        env: 'NODE_ENV',
        arg: 'node-env'
    },
    host: {
        default: 'localhost',
        env: 'APP_HOST',
        doc: 'Application host',
        format: String
    },
    port: {
        default: 3000,
        env: 'APP_PORT',
        doc: 'Application port',
        format: Number
    },
    logLevel: {
        default: 'debug',
        env: 'LOG_LEVEL',
        doc: 'Log level',
        format: String
    },
    database: {
        host: {
            default: 'localhost',
            env: 'APP_DB_HOST',
            doc: 'Database host',
            format: String
        },
        port: {
            default: 5432,
            env: 'APP_DB_PORT',
            doc: 'Database port',
            format: Number
        },
        username: {
            default: 'test',
            env: 'APP_DB_USERNAME',
            doc: 'Database username',
            format: String
        },
        password: {
            default: 'test',
            env: 'APP_DB_PASSWORD',
            doc: 'Database password',
            format: String
        },
        name: {
            default: 'test',
            env: 'APP_DB_NAME',
            doc: 'Database name',
            format: String
        },
        ssl: {
            default: 'false',
            env: 'APP_DB_SSL',
            doc: 'Is SSL connection',
            format: Boolean
        },
        connectionTimeout: {
            default: 1200000,
            env: 'APP_DB_TIMEOUT',
            doc: 'idleTimeoutMillis',
            format: Number
        },
        poolSize: {
            default: 50,
            env: 'APP_DB_POOLSIZE',
            doc: 'Connection pool size',
            format: Number
        }
    }
});

values.validate({ allowed: 'strict' });

export const config = values.getProperties();
