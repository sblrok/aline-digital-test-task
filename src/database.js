import mysql from 'mysql';
import fs from 'fs';
import {config} from './config';
import util from 'util';
import {logger} from './logger';

export default class DataBase {
    constructor() {
        this.pool = mysql.createPool({
            connectionLimit: config.database.poolSize,
            host: config.database.host,
            user: config.database.username,
            password: config.database.password,
            database: config.database.name,
            multipleStatements: true
        });
        this.pool.on('connection', function (connection) {
            connection.on('enqueue', function (sequence) {
                if ('Query' === sequence.constructor.name) {
                    logger.debug(sequence.sql);
                }
            });
        });
    }

    async getConnection() {
        return new Promise((resolve, reject) =>
            this.pool.getConnection((err, connection) => {
                if (err) {
                    reject(err);
                } else {
                    // you may try to use bluebird.promisifyAll
                    connection.beginTransactionAsync = util.promisify(connection.beginTransaction).bind(connection);
                    connection.commitAsync = util.promisify(connection.commit).bind(connection);
                    connection.rollbackAsync = util.promisify(connection.rollback).bind(connection);
                    connection.queryAsync = util.promisify(connection.query).bind(connection);
                    resolve(connection);
                }
            })
        )
    }

    // todo: use existing migration solution
    // find a solution in npm or use something like liquibase
    async migrate() {
        const connection = await this.getConnection();
        await connection.beginTransactionAsync();

        try {
            await connection.queryAsync('CREATE TABLE IF NOT EXISTS migrations ( name TEXT )');
            const last = await connection.queryAsync('SELECT * FROM migrations ORDER BY name DESC LIMIT 1');
            const lastTimestamp = last[0] && last[0].name.split('_')[0];

            const path = __dirname + '/../migrations';
            const files = fs.readdirSync(path);
            for (let file of files) {
                const timestamp = file.split('_')[0];
                if (lastTimestamp >= timestamp) {
                    break;
                }

                const migration = require(path + '/' + file);
                await connection.queryAsync(migration.up);
                await connection.queryAsync(`INSERT INTO migrations ( name ) VALUES ( "${file}" )`);
            }
            await connection.commitAsync();
        } catch (err) {
            logger.error(err);
            await connection.rollbackAsync();
            throw err;
        } finally {
            connection.release();
        }
    }
}
