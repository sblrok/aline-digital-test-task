export default class HttpError extends Error {
    constructor(type, message) {
        super();
        this.code = type && type.code || 500;
        this.message = message || type && type.message || 'Internal Server Error';
    }
}
