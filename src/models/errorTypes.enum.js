export const ErrorTypes = Object.freeze({
    NotFound: { code: 404, message: 'Not Found' },
    BadRequest: { code: 400, message: 'Bad Request' },
    InternalServerError: { code: 500, message: 'Internal Server Error' },
    ForbiddenError: { code: 403, message: 'Forbidden' },
    MethodNotAllowedError: { code: 405, message: 'Method Not Allowed' },
    UnauthorizedError: { code: 403, message: 'Unauthorized' },
    NotAcceptableError: { code: 406, message: 'Not Acceptable Error' },
    ValidationError: { code: 422, message: 'Request entity too large' }
});
