import '@babel/polyfill'
import Koa from 'koa';
import booksRouter from './controllers/books.controller';
import bodyParser from 'koa-bodyparser';
import {config} from './config';
import {container} from "./common/container";
import Database from "./database";
import {logger} from './logger';
import errorHandlerMiddleware from "./services/errorHandler.middleware";

const app = new Koa();
const database = container.get(Database);

database.migrate()
    .then(() => logger.info('Migration completed'))
    .catch((err) => logger.error('Migration filed\n' + err));

app.use(errorHandlerMiddleware);
app.use(bodyParser());
app.use(booksRouter.routes());

const server = app.listen(config.port, () => {
    logger.info(`Server listening on port: ${config.port}`);
});

module.exports = server;
