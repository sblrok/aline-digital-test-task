import {Get, Post, Put, Delete, Controller} from "../common/routingDecorators";
import {container} from '../common/container';
import BooksService from "../services/books.service";

@Controller('/books')
class BooksController {
    constructor() {
        this.booksService = container.get(BooksService);
        this.baseLimit = 100;
    }
    @Get('/')
    async find(params, query) {
        const options = {
            offset: query.offset || 0,
            limit: query.limit || this.baseLimit,
            sortBy: query.sortBy,
            sortOrder: query.sortOrder || 'ASC'
        };

        return this.booksService.find(options);
    }

    @Get('/:id')
    findOne(params) {
        const options = {
            id: params.id
        };

        return this.booksService.findOne(options);
    }

    @Post('/')
    create(params, query, body) {
        return this.booksService.create(body);
    }

    @Put('/:id')
    update(params, query, body) {
        return this.booksService.update(params.id, body);
    }

    @Delete('/:id', {allowEmptyResponse: true})
    delete(params) {
        return this.booksService.delete(params.id);
    }

    @Get('/info/count')
    async getCount() {
        return this.booksService.getCount();
    }

    @Post('/find')
    findBy(params, query, body) {
        const options = {
            offset: query.offset || body.offset || 0,
            limit: query.limit || body.limit || this.baseLimit,
            sortBy: query.sortBy || body.sortBy,
            sortOrder: query.sortOrder || body.sortOrder || 'ASC',
            findKey: body.findKey,
            findValue: body.findValue
        };

        return this.booksService.find(options);
    }
}

const books = new BooksController();
export default books.router;
