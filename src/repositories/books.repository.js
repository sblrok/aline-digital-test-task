import {Connection, Repository} from "../common/connectionDecorators";
import {ErrorTypes} from "../models/errorTypes.enum";
import HttpError from "../models/httpError.model";
import DataBase from "../database";

@Repository(DataBase)
export default class BooksRepository {
    @Connection()
    async find(connection, options) {
        let selectSql = `SELECT * FROM books`;
        if (options.findKey && options.findValue) {
            selectSql += ` WHERE ${options.findKey} LIKE '%${options.findValue}%'`;
        }
        if (options.sortBy) {
            selectSql += ` ORDER BY ${options.sortBy} ${options.sortOrder}`;
        }
        selectSql += ` LIMIT ${options.limit} OFFSET ${options.offset}`;

        return connection.queryAsync(selectSql);
    }

    @Connection()
    async findOne(connection, options) {
        const selectSql = 'SELECT * FROM books WHERE id = ? LIMIT 1';
        const selectParams = [options.id];
        const result = await connection.queryAsync(selectSql, selectParams);

        return result[0];
    }

    @Connection({useTransaction: true})
    async create(connection, options) {
        const selectSql = 'SELECT id FROM books WHERE TITLE = ?';
        const selectParams = [options.title];
        const existed = await connection.queryAsync(selectSql, selectParams);
        if (existed.length) {
            throw new HttpError(ErrorTypes.BadRequest, 'Book already exist');
        }

        const insertSql = 'INSERT INTO books ( title, date, author, description, image ) ' +
                          'VALUES ( ?, ?, ?, ?, ? )';
        const insertParams = [
            options.title,
            options.date,
            options.author,
            options.description,
            options.image
        ];
        const insertResult = await connection.queryAsync(insertSql, insertParams);

        return {
            id: insertResult.insertId,
            title: options.title,
            date: options.date,
            author: options.author,
            description: options.description,
            image: options.image
        };
    }

    @Connection({useTransaction: true})
    async update(connection, id, data) {
        const selectSql = 'SELECT id FROM books WHERE id = ?';
        const selectParams = [id];
        const existed = await connection.queryAsync(selectSql, selectParams);
        if (!existed.length) {
            throw new HttpError(ErrorTypes.NotFound, 'Book does not exist');
        }

        const selectUniqueSql = 'SELECT id FROM books WHERE TITLE = ?';
        const selectUniqueParams = [data.title];
        const existedUnique = await connection.queryAsync(selectUniqueSql, selectUniqueParams);
        if (existedUnique.length) {
            throw new HttpError(ErrorTypes.BadRequest, 'Book already exist');
        }

        const sql = 'UPDATE books SET ' +
                    'title = ?, date = ?, author = ?, description = ?, image = ? ' +
                    'WHERE id = ?';
        const params = [
            data.title,
            data.date,
            data.author,
            data.description,
            data.image,
            id
        ];
        await connection.queryAsync(sql, params);

        return {
            id,
            title: data.title,
            date: data.date,
            author: data.author,
            description: data.description,
            image: data.image
        };
    }

    @Connection({useTransaction: true})
    async delete(connection, options) {
        const selectSql = 'SELECT id FROM books WHERE id = ?';
        const selectParams = [options.id];
        const existed = await connection.queryAsync(selectSql, selectParams);
        if (!existed.length) {
            throw new HttpError(ErrorTypes.NotFound, 'Book does not exist');
        }

        const deleteSql = 'DELETE FROM books WHERE id = ?';
        const deleteParams = [options.id];
        await connection.queryAsync(deleteSql, deleteParams);
    }

    @Connection()
    async getCount(connection) {
        const selectSql = `SELECT COUNT(id) FROM books`;
        const result = await connection.queryAsync(selectSql);
        return result[0]['COUNT(id)'];
    }
}
