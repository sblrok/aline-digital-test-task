import Router from '@koa/router';
import {ErrorTypes} from '../models/errorTypes.enum';
import HttpError from '../models/httpError.model';
import {logger} from '../logger';

//<editor-fold desc="REST decorators">
/**
 * Decorator for controller URL initialization
 *
 * Sets base URL to all class methods
 *
 * @param {string} route Base URL
 */
export function Controller(route) {
    return function(descriptor) {
        const {kind, elements} = descriptor;
        elements.unshift({
            kind: 'field',
            key: '__baseRoute',
            placement: 'own',
            descriptor: { configurable: true, writable: true, enumerable: false },
            initializer: () => route
        });
        return descriptor;
    };
}

/**
 * GET method handler
 *
 * Decorator indicates that the function will receive a GET request at the specified route
 *
 * @param {string}  route                      URL where function response is expected
 * @param {Object}  [options]                    Decorator options
 * @param {boolean} options.allowEmptyResponse Allows the function to return an empty response. Otherwise returns 404
 */
export function Get(route, options) {
    return routeDecorator('GET', route, options);
}

/**
 * POST method handler
 *
 * Decorator indicates that the function will receive a POST request at the specified route
 *
 * @param {string}  route                      URL where function response is expected
 * @param {Object}  [options]                    Decorator options
 * @param {boolean} options.allowEmptyResponse Allows the function to return an empty response. Otherwise returns 404
 */
export function Post(route, options) {
    return routeDecorator('POST', route, options);
}

/**
 * PUT method handler
 *
 * Decorator indicates that the function will receive a PUT request at the specified route
 *
 * @param {string}  route                      URL where function response is expected
 * @param {Object}  [options]                    Decorator options
 * @param {boolean} options.allowEmptyResponse Allows the function to return an empty response. Otherwise returns 404
 */
export function Put(route, options) {
    return routeDecorator('PUT', route, options);
}

/**
 * DELETE method handler
 *
 * Decorator indicates that the function will receive a DELETE request at the specified route
 *
 * @param {string}  route                      URL where function response is expected
 * @param {Object}  [options]                    Decorator options
 * @param {boolean} options.allowEmptyResponse Allows the function to return an empty response. Otherwise returns 404
 */
export function Delete(route, options) {
    return routeDecorator('DELETE', route, options);
}
//</editor-fold>

function routeDecorator(method, route, options) {
    options = options || {};

    return function (descriptor) {
        if (!descriptor || descriptor.kind !== 'method') {
            return descriptor;
        }
        return {
            kind: 'field',
            key: descriptor.key,
            placement: 'own',
            initializer: routeInitializer(
                descriptor.descriptor.value,
                method,
                route,
                options
            ),
            descriptor: {
                configurable: true,
                writable: true,
                enumerable: false
            }
        };
    }
}

function routeInitializer(func, method, route, options) {
    return function () {
        const fullRoute = (this.__baseRoute || '') + route;
        const bindedValue = requestMiddleware(func, options).bind(this);
        if (!this.router) {
            this.router = new Router();
        }
        if (method === 'GET') {
            this.router.get(fullRoute, bindedValue);
        } else if (method === 'POST') {
            this.router.post(fullRoute, bindedValue);
        } else if (method === 'PUT') {
            this.router.put(fullRoute, bindedValue);
        } else if (method === 'DELETE') {
            this.router.del(fullRoute, bindedValue);
        }

        return bindedValue;
    }
}

/**
 * Request adapter
 *
 * Changes arguments from (ctx, next) to (params, query, body, ctx)
 * Using options
 *
 * @param {Function} func                       Wrapped function
 * @param {Object}   [options]                    Decorator options
 * @param {boolean}  options.allowEmptyResponse Allows the function to return an empty response. Otherwise returns 404
 */
function requestMiddleware(func, options) {
    return async function (ctx, next) {
        logger.debug(
            `${ctx.request.method} REQUEST:\n` +
            `URL: ${ctx.request.url}\n` +
            `PARAMS: ${JSON.stringify(ctx.params)}\n` +
            `QUERY: ${JSON.stringify(ctx.request.query)}\n` +
            `BODY: ${JSON.stringify(ctx.request.body)}`
        );

        const result = await func.bind(this)(
            ctx.params,
            ctx.request.query,
            ctx.request.body,
            ctx);

        if (!result && result !== 0) {
            if (!options.allowEmptyResponse) {
                throw new HttpError(ErrorTypes.NotFound);
            }
            ctx.status = 204;
        } else {
            ctx.status = 200;
            ctx.body = result;
        }
        next();
    }
}
