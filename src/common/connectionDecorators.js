import {container} from './container';
import PoolConnection from 'mysql/lib/PoolConnection'
import {logger} from "../logger";

/**
 * Decorator sets database instance to the class
 *
 * Sets database instance using "container"
 * allows to use @Connection decorator
 *
 * @param {object} DBClass Database class
 */
export function Repository(DBClass) {
    return function(descriptor) {
        const {kind, elements} = descriptor;
        elements.unshift({
            kind: 'field',
            key: 'database',
            placement: 'own',
            descriptor: { configurable: true, writable: true, enumerable: true },
            initializer: () => container.get(DBClass)
        });
        return descriptor;
    };
}

/**
 * Use connection middleware
 *
 * Sets database connection in first argument of the function
 *
 * @param {Object}   [options]              Decorator options
 * @param {boolean}  options.useTransaction Uses transaction for created connection
 */
export function Connection(options) {
    return function (descriptor) {
        if (!descriptor || descriptor.kind !== 'method') {
            return descriptor;
        }
        return {
            kind: 'field',
            key: descriptor.key,
            placement: 'own',
            initializer: getConnectionMiddleware(descriptor.descriptor.value, options),
            descriptor: {
                configurable: true,
                writable: true,
                enumerable: false
            }
        };
    }
}

function getConnectionMiddleware(func, options) {
    return function() {
        if (!this.database) {
            logger.warn(
                'Use should use @Repository decorator\n' +
                'or declare this.database for this class'
            );
        }

        return async function (...args) {
            if (args[0] instanceof PoolConnection) {
                return func.bind(this)(...args);
            }

            const connection = await this.database.getConnection();
            if (options && options.useTransaction) {
                try {
                    await connection.beginTransactionAsync();
                    const result = await func.bind(this)(connection, ...args);
                    await connection.commitAsync();
                    return result;
                } catch (err) {
                    await connection.rollbackAsync();
                    throw err;
                } finally {
                    connection.release();
                }
            } else {
                return func.bind(this)(connection, ...args);
            }
        }
    }
}
