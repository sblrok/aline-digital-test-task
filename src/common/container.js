class Container {
    instances = {};

    get(Class) {
        if (!this.instances[Class]) {
            this.instances[Class.name] = new Class();
        }
        return this.instances[Class.name];
    }
}

export const container = new Container();
