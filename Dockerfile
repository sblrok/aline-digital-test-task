### STAGE 1: Build ###
FROM node:lts-slim as builder

COPY package.json ./
RUN npm i && mkdir /ng-app && mv ./node_modules ./ng-app
WORKDIR /ng-app
COPY . .
RUN npm run build

### STAGE 2: Setup ###

FROM node:lts-slim
RUN mkdir build/
COPY --from=builder /ng-app ./build
WORKDIR /build
EXPOSE 3000:3000
CMD [ "node", "dist/index.js" ]
